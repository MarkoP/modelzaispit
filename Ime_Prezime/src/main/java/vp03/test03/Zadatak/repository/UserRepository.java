package vp03.test03.Zadatak.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import vp03.test03.Zadatak.model.user.SecurityUser;

public interface UserRepository extends JpaRepository<SecurityUser, Long> {
	public SecurityUser findByUsername(String username);
	
}
